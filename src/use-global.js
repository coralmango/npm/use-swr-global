"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var swr_1 = __importDefault(require("swr"));
var useGlobal = function (key) {
    var _a = swr_1.default("global:" + key, null, {
        initialData: null,
    }), data = _a.data, mutate = _a.mutate;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    var setData = function (value) {
        mutate(value, false);
    };
    return [data, setData];
};
exports.default = useGlobal;
