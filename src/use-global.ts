import useSWR from "swr"

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type HookReturnValue = [any, (value: any) => void]

const useGlobal = (key: string): HookReturnValue => {
  const { data, mutate } = useSWR(`global:${key}`, null, {
    fallbackData: null,
  })

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const setData = (value: any) => {
    mutate(value, false)
  }

  return [data, setData]
}

export default useGlobal
